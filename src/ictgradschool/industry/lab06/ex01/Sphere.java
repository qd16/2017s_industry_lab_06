package ictgradschool.industry.lab06.ex01;
/**
 * Should represent a Sphere.
 *
 */
public class Sphere {
    private double radius;
    private double[] centroid;
    private static int count = 0;

    public Sphere(double r){
        this.radius = r;
        count++;
    }

    public int count(){
        return count;
    }

    public double surfaceArea(){
        return Math.PI * 4.0 * radius * radius;
    }

    public double volume(){
        return Math.PI * 4.0 / 3.0 * radius * radius * radius;
    }
}
